# Microservice-demo
## Overview:
Porject has some URLs open for everyone, and some requiring oauth access. 
Oauth is using [okta](https://www.okta.com/#) account. Please register account to use secured endpoints

## Commands:
* Launch project in 1 command from anywhere: `docker run --detach --publish 8080:8080 --name microservice-demo registry.gitlab.com/danylo.stasenko/microservice-demo:latest`

* Start gitlab runner `docker exec db3 gitlab-runner start`
* Run springboot: `mvn spring-boot:run`
* Build jar: `mvn spring-boot:build-image`
* Run jar: `java -jar target/microservice-demo-0.0.1-SNAPSHOT.jar`

* Build Docker image: `docker build --tag microservice-demo:1.0-SNAPSHOT .`
* Run Docker image: `docker run --detach --publish 8080:8080 --name microservice-demo microservice-demo:1.0-SNAPSHOT`
* Get into the container `docker exec -it d6eed0a5c633 sh` where `d6eed0a5c633` is CONTAINER ID

## Project URLs
* Not secured home page `http://localhost:8080/`
* Secured page: `http://localhost:8080/restricted`
* User details: `http://localhost:8080/user/oauthinfo`

## Project tasks:
Done:
* Add basic spring boot project ('helloword') = 2h
* Add & test octa integration = 3h
* Add gitignore = 1h
* Add Dockerfile = 2h
* Add gitlab.ci integration + runners config = 4h 
* Add support fot building docker images = 2h

todo:
* Add API for some records
* Add database CRUD (JPA)
* Add paging & sorting repository
* Add swagger
* Add docker-compose file to run with DB
* Add tests, jacoco
* Add google/facebook auth
* Add readme with screenshots
* Add postman collection
* Add UI project on Angular


# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/maven-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Security](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/reference/htmlsingle/#boot-features-security)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
* [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
* [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)
