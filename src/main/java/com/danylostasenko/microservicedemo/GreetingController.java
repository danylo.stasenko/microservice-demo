package com.danylostasenko.microservicedemo;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    @RequestMapping("/")
    @ResponseBody
    public String home() {
        return "Welcome home!";
    }

    @RequestMapping("/restricted")
    @ResponseBody
    public String restricted(@AuthenticationPrincipal OAuth2User oauth2User) {
        String name = ((DefaultOidcUser) oauth2User).getClaimAsString("name");
        return "You found the secret lair, " + name;
    }
}
